<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/login', 'API\UsersController@login');
Route::post('/user/signup', 'API\UsersController@signUp');

Route::get('/cards', 'PokemonsController@index');
Route::get('/search', 'PokemonsController@search');
Route::get('/type', 'PokemonsController@getType');
Route::get('/rarity', 'PokemonsController@getRarity');
Route::get('/set', 'PokemonsController@getSet');
