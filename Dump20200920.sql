CREATE DATABASE  IF NOT EXISTS `onlinebetting` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `onlinebetting`;
-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: onlinebetting
-- ------------------------------------------------------
-- Server version	5.7.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `amount_types`
--

DROP TABLE IF EXISTS `amount_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `amount_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(15,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amount_types`
--

LOCK TABLES `amount_types` WRITE;
/*!40000 ALTER TABLE `amount_types` DISABLE KEYS */;
INSERT INTO `amount_types` VALUES (1,2.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,25.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,50.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(4,250.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(5,500.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(6,1000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(7,5000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(8,10000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(9,50000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(10,80000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(11,100000.00,'2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `amount_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bet_game_horse`
--

DROP TABLE IF EXISTS `bet_game_horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_game_horse` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bet_id` bigint(20) unsigned NOT NULL,
  `game_id` bigint(20) unsigned NOT NULL,
  `horse_id` bigint(20) unsigned NOT NULL,
  `position_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bet_game_horse_bet_id_foreign` (`bet_id`),
  KEY `bet_game_horse_game_id_foreign` (`game_id`),
  KEY `bet_game_horse_horse_id_foreign` (`horse_id`),
  CONSTRAINT `bet_game_horse_bet_id_foreign` FOREIGN KEY (`bet_id`) REFERENCES `bets` (`id`),
  CONSTRAINT `bet_game_horse_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `bet_game_horse_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet_game_horse`
--

LOCK TABLES `bet_game_horse` WRITE;
/*!40000 ALTER TABLE `bet_game_horse` DISABLE KEYS */;
INSERT INTO `bet_game_horse` VALUES (189,77,1,4,5),(190,77,1,2,6),(191,77,2,3,3),(192,77,2,4,4),(193,78,1,4,5),(194,78,1,2,6),(195,78,2,3,3),(196,78,2,4,4),(197,79,1,4,5),(198,79,1,2,6),(199,79,2,3,3),(200,79,2,4,4),(201,80,1,4,5),(202,80,1,2,6),(203,80,2,3,3),(204,80,2,4,4),(205,81,1,4,5),(206,81,1,2,6),(207,81,2,3,3),(208,81,2,4,4),(209,82,1,1,1),(210,83,1,1,1);
/*!40000 ALTER TABLE `bet_game_horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bet_types`
--

DROP TABLE IF EXISTS `bet_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formula` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `game_count` int(11) NOT NULL DEFAULT '1',
  `horse_count` int(11) NOT NULL DEFAULT '1',
  `select_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `select_input` json NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bet_types`
--

LOCK TABLES `bet_types` WRITE;
/*!40000 ALTER TABLE `bet_types` DISABLE KEYS */;
INSERT INTO `bet_types` VALUES (1,'WIN',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,1,'each','[1]','#e1b304',1),(2,'FORECAST',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,2,'each','[1, 2]','#9bbdbc',0),(3,'QUARTET',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,4,'each','[1, 2, 3, 4]','#cf7c27',0),(4,'PENTAFECTA',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,5,'each','[1, 2, 3, 4, 5]','#536c81',0),(5,'SUPER 6',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',1,6,'each','[1, 2, 3, 4, 5, 6]','#6abcd9',0),(6,'DAILY DOUBLE',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',2,1,'each','[1]','#11a8fc',0),(7,'WINNER TAKE ALL',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',7,1,'each','[1]','#8fec11',1),(11,'PICK SIX',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',6,1,'each','[1]','#513133',0),(12,'PICK FIVE',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10',5,1,'each','[1]','#36038f',0);
/*!40000 ALTER TABLE `bet_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bets`
--

DROP TABLE IF EXISTS `bets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PHP',
  `win_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `winning_amount` decimal(15,2) DEFAULT '0.00',
  `event_id` bigint(20) unsigned NOT NULL,
  `bet_type_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bets_id_unique` (`id`),
  KEY `bets_user_id_foreign` (`user_id`),
  KEY `bets_event_id_foreign` (`event_id`),
  KEY `bets_bet_type_id_foreign` (`bet_type_id`),
  CONSTRAINT `bets_bet_type_id_foreign` FOREIGN KEY (`bet_type_id`) REFERENCES `bet_types` (`id`),
  CONSTRAINT `bets_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  CONSTRAINT `bets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bets`
--

LOCK TABLES `bets` WRITE;
/*!40000 ALTER TABLE `bets` DISABLE KEYS */;
INSERT INTO `bets` VALUES (77,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:26:54','2020-09-16 15:26:54',0.00,1,6),(78,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:34:28','2020-09-16 15:34:28',0.00,1,6),(79,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:57:37','2020-09-16 15:57:37',0.00,1,6),(80,1,0.20,'PHP','Pending',NULL,'2020-09-16 15:57:56','2020-09-16 15:57:56',0.00,1,6),(81,1,0.20,'PHP','Pending',NULL,'2020-09-16 16:04:31','2020-09-16 16:04:31',0.00,1,6),(82,1,100.00,'PHP','Pending',NULL,'2020-09-16 16:53:58','2020-09-16 16:53:58',0.00,1,1),(83,1,100.00,'PHP','Pending',NULL,'2020-09-16 16:54:33','2020-09-16 16:54:33',0.00,1,1);
/*!40000 ALTER TABLE `bets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `claim_requests`
--

DROP TABLE IF EXISTS `claim_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `claim_requests` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_processed` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'requested',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `claim_requests`
--

LOCK TABLES `claim_requests` WRITE;
/*!40000 ALTER TABLE `claim_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `claim_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'STA. MESA','Description 1','2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,'Carmona','Description 2','2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,'STA. ANA','Description 3','2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_horse`
--

DROP TABLE IF EXISTS `game_horse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `game_horse` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) unsigned NOT NULL,
  `horse_id` bigint(20) unsigned NOT NULL,
  `horse_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `game_horse_game_id_foreign` (`game_id`),
  KEY `game_horse_horse_id_foreign` (`horse_id`),
  CONSTRAINT `game_horse_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `game_horse_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_horse`
--

LOCK TABLES `game_horse` WRITE;
/*!40000 ALTER TABLE `game_horse` DISABLE KEYS */;
INSERT INTO `game_horse` VALUES (1,1,1,1),(2,1,2,2),(3,1,3,3),(4,1,4,4),(5,1,5,5),(6,1,6,6),(7,2,1,1),(8,2,4,2),(9,2,3,3),(10,2,8,4),(11,2,7,5),(27,3,1,1),(28,3,4,2),(29,3,3,3),(30,3,8,4),(31,3,7,5),(32,4,1,1),(33,4,4,2),(34,4,3,3),(35,4,8,4),(36,4,7,5),(37,5,1,1),(38,5,4,2),(39,5,3,3),(40,5,8,4),(41,5,7,5),(62,11,1,1),(63,11,4,2),(64,11,3,3),(65,11,8,4),(66,11,7,5),(67,12,1,1),(68,12,4,2),(69,12,3,3),(70,12,8,4),(71,12,7,5);
/*!40000 ALTER TABLE `game_horse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `games` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `generate_winners_lock` tinyint(1) NOT NULL DEFAULT '0',
  `other_information` json DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `livestream_url` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bet_types` json DEFAULT NULL,
  `odd_types` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `event_id` bigint(20) unsigned DEFAULT NULL,
  `game_number` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `games_event_id_foreign` (`event_id`),
  CONSTRAINT `games_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,'R1',NULL,'2020-09-16 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:11:27','2020-09-16 12:11:27',1,1),(2,'R2',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,2),(3,'R3',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,3),(4,'R4',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,4),(5,'R5',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,5),(11,'R6',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,6),(12,'R7',NULL,'2020-09-14 00:00:00',NULL,'PHT',0,NULL,NULL,NULL,'pending',NULL,NULL,'2020-09-16 12:12:05','2020-09-16 12:12:05',1,7);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horse_images`
--

DROP TABLE IF EXISTS `horse_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horse_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `horse_id` bigint(20) unsigned NOT NULL,
  `image_path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `horse_images_horse_id_foreign` (`horse_id`),
  CONSTRAINT `horse_images_horse_id_foreign` FOREIGN KEY (`horse_id`) REFERENCES `horses` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horse_images`
--

LOCK TABLES `horse_images` WRITE;
/*!40000 ALTER TABLE `horse_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `horse_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horses`
--

DROP TABLE IF EXISTS `horses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `statistics` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sire` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dam` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trainer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_prize_money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wins` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `intro` text COLLATE utf8mb4_unicode_ci,
  `career` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horses`
--

LOCK TABLES `horses` WRITE;
/*!40000 ALTER TABLE `horses` DISABLE KEYS */;
INSERT INTO `horses` VALUES (1,'A SHIN MARCHING','3yo DK B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Eishin Flash (JPN) 2007','Massillon Girl (USA) 2003 [By With Approval (CAN) 1986]','Yasuyuki Takahashi','Eishindo Co. Ltd.','Japan : JPY ¥366,000','1400m (1)','A Shin Marching is a thoroughbred horse born in Japan in 2017. Race horse A Shin Marching is by Eishin Flash (JPN) out of Massillon Girl (USA) , trained by Yasuyuki Takahashi. A Shin Marching form is available here. Owned by Eishindo Co. Ltd..','1 Win (11%) - No Places (11%) - 9 starts',NULL),(2,'ABRACADAZZLE','3yo B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Sizzling (AUS) 2009','	Abracadash (AUS) 2011 [By Magic Albert (AUS) 1998]','Tony Gollan','Mrs D M Power & Mrs L K Barton','AUD $21,200','1400m (1)','Abracadazzle is a thoroughbred horse born in Australia in 2017. Race horse Abracadazzle is by Sizzling (AUS) out of Abracadash (AUS) , trained by Tony Gollan. Abracadazzle form is available here. Owned by Mrs D M Power & Mrs L K Barton.','1 Win (33%) - No Places (33%) - 3 starts',NULL),(3,'LUCKY IMAGINATION','3yo GR/RO Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Imagining (USA) 2008','Lucky Chick (USA)2000 [By Two Punch (USA) 1983]','Patricia Farro','Vaccaro Racing Stable & Patricia Farro','AUD $43,970','1408m (1)','Lucky Imagination is a thoroughbred horse born in United States of America in 2017. Race horse Lucky Imagination is by Imagining (USA) out of Lucky Chick (USA) , trained by Patricia Farro. Lucky Imagination form is available here. Owned by VACCARO RACING STABLE & PATRICIA FARRO.','1 Win (33%) - No Places (33%) - 3 starts',NULL),(4,'MAMA\'S COFFEE','2yo B Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Casino Drive (USA) 2005','Mama\'s Dish (JPN) 2007','Noboru Yonekawa','Noboru Yonekawa','AUD $12,846','1200m (1)','Mama\'s Coffee is a thoroughbred horse born in Japan in 2018. Race horse Mama\'s Coffee is by Casino Drive (USA) out of Mama\'s Dish (JPN) , trained by Noboru Yonekawa. Mama\'s Coffee form is available here. Owned by .','1 Win (33%) - 2 Placings (100%) - 3 starts',NULL),(5,'BEAR THE CROWN','4yo CH Mare',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Olympic Glory (IRE) 2010','Mosscedes (AUS) 2006 [By Mossman (AUS) 1995]','Barry Ratcliff','B J Ratcliff & S M Foster','AUD $29,400','1000m (1), 1500m (1)','Bear The Crown is a thoroughbred horse born in Australia in 2016. Race horse Bear The Crown is by Olympic Glory (IRE) out of Mosscedes (AUS) , trained by Barry Ratcliff. Bear The Crown form is available here. Owned by B J Ratcliff & S M Foster.','2 Wins (29%) - 1 Place (43%) - 7 starts',NULL),(6,'CAMPARI SODA','5yo B Mare',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Dream Ahead (USA) 2008','Chitraka (AUS) 2007 [By Tale of The Cat (USA) 1994]','Barry Ratcliff','B J Ratcliff & A J Ahearn','AUD $55,920','1000m (1), 1106m (1), 1250m (1)','Campari Soda is a thoroughbred horse born in Australia in 2015. Race horse Campari Soda is by Dream Ahead (USA) out of Chitraka (AUS) , trained by Barry Ratcliff. Campari Soda form is available here. Owned by B J Ratcliff & A J Ahearn.','3 Wins (17%) - 6 Placings (50%) - 18 starts',NULL),(7,'PRETTY RUNAWAY','3yo SOR Filly',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Winners Award (USA) 2002','Pretty Jane Perry [By Pretty Boy Perry (USA) 1999]','Jason Pascoe','Heste Sport Inc','AUD $59,348','274m (1), 302m (1), 366m (1)','Pretty Runaway is a thoroughbred horse born in Canada in 2017. Race horse Pretty Runaway is by Winners Award (USA) out of Pretty Jane Perry , trained by Jason Pascoe. Pretty Runaway form is available here. Owned by HESTE SPORT INC.','3 Wins (33%) - 3 Placings (67%) - 9 starts',NULL),(8,'RESILIENCE BLUE','3yo DK B Colt',NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10','Unrivaled (JPN) 2006','B B Cygnus (JPN) 2002 [By White Muzzle (GB) 1990]','Masaki Matsuyama','Ygg Horse Club Co. Ltd.','AUD $39,496','1500m (1)','Resilience Blue is a thoroughbred horse born in Japan in 2017. Race horse Resilience Blue is by Unrivaled (JPN) out of B B Cygnus (JPN) , trained by Masaki Matsuyama. Resilience Blue form is available here. Owned by YGG Horse Club Co. Ltd..','1 Win (25%) - 1 Place (50%) - 4 starts',NULL);
/*!40000 ALTER TABLE `horses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2019_08_19_000000_create_failed_jobs_table',1),(9,'2020_09_03_121503_create_status_types_table',1),(10,'2020_09_03_132008_create_games_table',1),(11,'2020_09_03_132451_create_horses_table',1),(12,'2020_09_03_133522_create_bets_table',1),(13,'2020_09_03_133552_create_winners_table',1),(14,'2020_09_03_134945_create_horse_images_table',1),(15,'2020_09_05_063817_create_games_horses_table',1),(16,'2020_09_05_124142_alter_bet_table_add_winning_amount_column',1),(17,'2020_09_08_044647_create_events_table',1),(18,'2020_09_08_062249_alter_games_table_add_event_id_column',1),(19,'2020_09_08_074510_create_bet_types_table',1),(20,'2020_09_08_075054_create_amount_types_table',1),(21,'2020_09_09_144901_alter_horses_table_add_columns',1),(22,'2020_09_11_121454_create_claim_requests_table',1),(23,'2020_09_12_071358_alter_horses_table_add_logo_column',1),(24,'2020_09_13_032217_alter_game_horse_table_add_horse_no_column',1),(25,'2020_09_16_100211_alter_bet_type_table_add_game_count_and_horse_count',1),(26,'2020_09_16_104846_create_bet_game_horse_pivot_table',1),(27,'2020_09_16_112430_alter_bet_table_add_event_id_column',1),(28,'2020_09_19_060323_alter_games_table_add_game_number_column',2),(29,'2020_09_19_093141_alter_bet_types_table_add_select_type_select_input',2),(30,'2020_09_19_100352_alter_bet_types_table_add_color_column',3),(31,'2020_09_19_150648_alter_bet_types_add_enabled_column',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('00384f74038908d071b8a66a6ff670e8ac569c202c515926c94066ded3d30d67ee514f03d2472abf',1,1,'authToken','[]',1,'2020-09-16 17:24:03','2020-09-16 17:24:03','2021-09-16 17:24:03'),('00819709a9ce99f2fec33cf30c3c8067a6380ed50d1af714fbbdbb8c675887d2307735c418fd1e2f',1,1,'authToken','[]',0,'2020-09-18 14:37:21','2020-09-18 14:37:21','2021-09-18 14:37:21'),('02177c5477893d706cd37e8214f126c239832b400b2a82cf7fab84b5fea4be6b5a95fd0db54e466d',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('02238ca822944449f237b9d589eb5822c5a58ad9d5cf0217a18c91377969e6ec21f097df6f0ef015',1,1,'authToken','[]',1,'2020-09-16 15:51:16','2020-09-16 15:51:16','2021-09-16 15:51:16'),('04847b2b6959bbf51f9510a1af4ae566b4709b73ae0c409c7b83accd27cf70aecfd1e8ad04e014d0',1,1,'authToken','[]',1,'2020-09-16 13:34:54','2020-09-16 13:34:54','2021-09-16 13:34:54'),('048555d59057af8c24fcd629bb6bcdaf8d355f68273bbd578f648d7b1588cf63b7264f9b03e769a8',1,1,'authToken','[]',1,'2020-09-16 17:22:27','2020-09-16 17:22:27','2021-09-16 17:22:27'),('04f9253376c37c2928b559b678ed6ac854efc7a766d146ea06bae4815ddae060f0a394bb876b4bc7',1,1,'authToken','[]',0,'2020-09-17 12:20:59','2020-09-17 12:20:59','2021-09-17 12:20:59'),('064ebbe08b9e9e7454a8ac8ffde558a7a63eae4283b7a31c86f21853597a47d171c25e7044fb8605',1,1,'authToken','[]',1,'2020-09-16 16:49:08','2020-09-16 16:49:08','2021-09-16 16:49:08'),('087b6fe7a60a6ca579e06692619bf8c19d7760abad8c1c8db5a5c6c63c94b971b44d91c908bac485',1,1,'authToken','[]',1,'2020-09-16 13:40:28','2020-09-16 13:40:28','2021-09-16 13:40:28'),('09fb7d6915cba3b10df42c3c92233d85d1e98ea1c2f945bdef6275ad3ec152160e8216da9df48359',1,1,'authToken','[]',0,'2020-09-17 12:00:49','2020-09-17 12:00:49','2021-09-17 12:00:49'),('0bb657a2175bd7516e2c02ee2ac0c6887f7f2d1cce99da3134d61a74ca3aca4659cf562fa736f123',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('0be03d9f0e5f62b270fdcb44143878aa911e47d7e283d84aeba3802c37783a822df1cab84a83be90',1,1,'authToken','[]',1,'2020-09-16 17:24:48','2020-09-16 17:24:48','2021-09-16 17:24:48'),('0c072e733ca7103ddbe32544715b2ebdcdfdb2f06547ebdf77c8d3bde17bc75ebe2a18c2592767f8',1,1,'authToken','[]',0,'2020-09-17 11:18:30','2020-09-17 11:18:30','2021-09-17 11:18:30'),('0cbadd819b006534529721a5118e783c95284a516f60c51a421f044924f39bbd2632deb9b081074f',1,1,'authToken','[]',1,'2020-09-16 13:56:03','2020-09-16 13:56:03','2021-09-16 13:56:03'),('0d1a7ea630c1dc72a97dfa63e21068fb75ce2f305c4732c2cf7f1ab537c6a0892087271db9465555',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('0e3cdc6d9a59c3148b80460d76638be04600a6024d19fc98c7d8bdc1f5e173b547a750c6cb3debfb',1,1,'authToken','[]',1,'2020-09-16 17:29:06','2020-09-16 17:29:06','2021-09-16 17:29:06'),('0e3f7992d04cfb7d2af059a5b597dd9c28b7057bbadc0b68df9d463a76d04f957e4e15b890da9af0',1,1,'authToken','[]',1,'2020-09-16 16:34:44','2020-09-16 16:34:44','2021-09-16 16:34:44'),('0e4adcd09427d59ef16cd07e13c652706e574a04390077e060be76eb3172c6cbbf8cc35d818a1dc5',1,1,'authToken','[]',1,'2020-09-16 13:39:04','2020-09-16 13:39:04','2021-09-16 13:39:04'),('0ee3e861e1bc61dcd78ec833417afa47dd929ff92679fd8999fd055b1ca76f2afc774617688df8a0',1,1,'authToken','[]',1,'2020-09-16 16:49:09','2020-09-16 16:49:09','2021-09-16 16:49:09'),('0f72b4d6a02d90ee9152a7e2ed26f8f792b2e73388a87bc6b88d647a6f0b6b8896041d68be47a9e3',1,1,'authToken','[]',1,'2020-09-16 12:10:11','2020-09-16 12:10:11','2021-09-16 12:10:11'),('101b13072e3f4a8929a5963229aa13fa2b7208c49ae67679e772903372735061b7f6d108c853d742',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('1165d946e0b6f0c2ba96a220d3a9debfa0e5eba78bbc0853a8e2c77242c70dba1c12f54e0e61afb3',1,1,'authToken','[]',1,'2020-09-16 17:28:59','2020-09-16 17:28:59','2021-09-16 17:28:59'),('134d1940fbde2d651d7724c4ffd3bd96761fd8b0bd575f549aa2091538575ba28ac6e3f583c575fd',1,1,'authToken','[]',1,'2020-09-16 17:29:00','2020-09-16 17:29:00','2021-09-16 17:29:00'),('13b509f552867ea272a9f856a8a391136633e1ed144da01458d0ed56df42acb23073dc1b87f84cc9',1,1,'authToken','[]',1,'2020-09-16 12:10:08','2020-09-16 12:10:08','2021-09-16 12:10:08'),('17ca9f2ffbe585e5f8054d55782383db883a1e5e0fe13aa0d656cb4441f3e854d70f7ac61c2222ba',1,1,'authToken','[]',1,'2020-09-16 16:34:31','2020-09-16 16:34:31','2021-09-16 16:34:31'),('1811ab4f140aaa2f44ae4270a48f8786fd397d8fb8d873e0d2d5e78f5d3499a6e1549003bfde9ee8',1,1,'authToken','[]',1,'2020-09-16 13:54:29','2020-09-16 13:54:29','2021-09-16 13:54:29'),('1841cb118de0dfd4f14aba25d4367c8b777ab247139078b90adf517c0f17bf7f0f3d9ddcd55d70ea',1,1,'authToken','[]',1,'2020-09-16 16:34:31','2020-09-16 16:34:31','2021-09-16 16:34:31'),('19ef8cbcbe5e5646edc477fe03153ffb694b24c8a3efd7175793d3bf75d0cdab3658df30c79e1169',1,1,'authToken','[]',1,'2020-09-16 16:49:06','2020-09-16 16:49:06','2021-09-16 16:49:06'),('1a9c0bdbe2789a7c1fab3ce59dd9fb1773a3b34b92823e506e4527450986dbbf9307d5264cf5b2ca',1,1,'authToken','[]',1,'2020-09-16 13:57:09','2020-09-16 13:57:09','2021-09-16 13:57:09'),('1d468e91e8548478f8b83c151ffade33037ce931d350122986570cc9c2694f66428a4f59b5b6ce4f',1,1,'authToken','[]',1,'2020-09-16 16:43:54','2020-09-16 16:43:54','2021-09-16 16:43:54'),('1d58a29766daa4261f71298997ab7d0e11ee51f297e350432fb36af3da998a35f427633600ada29a',1,1,'authToken','[]',1,'2020-09-16 17:19:41','2020-09-16 17:19:41','2021-09-16 17:19:41'),('1db2f672bdf91370fddc29e734f0bf6e6b2f35357f9215c1c70326e5e6ab0b2d9ffd7ab9d42d2fc7',1,1,'authToken','[]',0,'2020-09-18 04:29:02','2020-09-18 04:29:02','2021-09-18 04:29:02'),('1df2b17d486470a2c9bd965e55ad1f705288f2517c52fdaeff7c403d83fece88928b1c04f6c16c48',1,1,'authToken','[]',0,'2020-09-18 04:29:06','2020-09-18 04:29:06','2021-09-18 04:29:06'),('1e45b8e084576a53a9d8e78fd198a74b34872aa80d17267a569e22fb552d9ac27cc0d11a78eaf4d0',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('1f0bb54dc3856b7adc39ad91f4bc3100a64556aff17ea68b247e47aac60458c164ff1b725f7e4844',1,1,'authToken','[]',1,'2020-09-16 17:26:54','2020-09-16 17:26:54','2021-09-16 17:26:54'),('1f13453f353a18227778be2dc4261f02ebfcff1db8b02cb7c904efbf21b11de09ad3a2aa62b7ca9a',1,1,'authToken','[]',1,'2020-09-16 17:30:43','2020-09-16 17:30:43','2021-09-16 17:30:43'),('1f44d0bcce0a64c6461e9c1b6a0ba730b012cf34726a463db3b87f7d7d59f8160ccdf1e31d2920ca',1,1,'authToken','[]',1,'2020-09-16 17:28:48','2020-09-16 17:28:48','2021-09-16 17:28:48'),('202b95b2e855a0204bdab929418fd6e964f177fac7317fcb9b0589b5180f07e80f6219725faa25ed',1,1,'authToken','[]',1,'2020-09-16 17:16:10','2020-09-16 17:16:10','2021-09-16 17:16:10'),('205fc2714421fbc4db7113e9fa6044558ff435a8a3906073a95c0a7c24f0b5d30b20a24d0e131956',1,1,'authToken','[]',1,'2020-09-16 17:17:40','2020-09-16 17:17:40','2021-09-16 17:17:40'),('20bfd104817265dd678c2759371012aebf0b0e5bcaf3ed1cd603d1e929e6e830330c94ec92aa9777',1,1,'authToken','[]',1,'2020-09-17 01:55:43','2020-09-17 01:55:43','2021-09-17 01:55:43'),('20e89a083cd526f2979cd96e222eeef6f7d0d95fc8077b7f3706022fcb1b7a7fc6217f6178b03bc3',1,1,'authToken','[]',0,'2020-09-18 13:38:24','2020-09-18 13:38:24','2021-09-18 13:38:24'),('213b4c39cd8966b17e3edda04700fc454106b3ff562b6d896e467512db3e8e167c0100c56a5779b8',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('218268eef13689948a6cdafcc15b4c0296aed6f3b9fab0ef320d6a1a5e9899e85347e525292a2f4a',1,1,'authToken','[]',1,'2020-09-16 13:35:02','2020-09-16 13:35:02','2021-09-16 13:35:02'),('21f6547a94888f73f19dde9298a4baa1aa57a0086abfe3128cae4fcefb988a53c6e63cbb29228a47',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('21fc8328cbdd23ecef11c6cf73e6f4c24a5666984a308032a1838a9f09b329fe7cbfd087065e8c5e',1,1,'authToken','[]',1,'2020-09-16 17:33:23','2020-09-16 17:33:23','2021-09-16 17:33:23'),('22a5652462594da1f8fb5ab840e52d9359f0f7daf00cbd9a4be8cea0975ad14cd3eedf4c25555ba8',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('22c5cb51d7fcd1fa63a843d0ccaefbcaeeaf4dd59d2ca14bb4ef03aece9c5f299342df10ee8801b3',1,1,'authToken','[]',0,'2020-09-18 04:29:03','2020-09-18 04:29:03','2021-09-18 04:29:03'),('239bea2f347ac0ecc623e07a880824ae1291a30e9c196f386f843f5df6ffdfa09a8c59995c223108',1,1,'authToken','[]',1,'2020-09-16 17:13:05','2020-09-16 17:13:05','2021-09-16 17:13:05'),('2462481b06c8016e380ac530bb31847ac835b2dac85b1de107c89895a537679b28d9cf98aea380fa',1,1,'authToken','[]',0,'2020-09-18 13:25:01','2020-09-18 13:25:01','2021-09-18 13:25:01'),('24c4893a9bb74542210f7e0a29fe602130beedb2741a3534a378b5f7dcd0216b1b583abc68bc1d81',1,1,'authToken','[]',0,'2020-09-18 13:35:09','2020-09-18 13:35:09','2021-09-18 13:35:09'),('24e01b939f66ee42718f7321ba8288f1fb1537ffdb49d1d1d814bee35a5f99ef0ec134d5e6b2baaf',1,1,'authToken','[]',0,'2020-09-18 01:01:56','2020-09-18 01:01:56','2021-09-18 01:01:56'),('251ce13365e109ca550c0010371727c6bc20252828b47766d8a7b0f7c1c0066d073b49abdf8dd44c',1,1,'authToken','[]',1,'2020-09-16 17:15:00','2020-09-16 17:15:00','2021-09-16 17:15:00'),('266d36f0749c03ca571956086ab29e816dd166a364405c8281f9ad631962420e8b119fb096486ae5',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('2707e6f5ec65d4adf6eaf67b4012014bedfb7319ed6ad62659c0e95a5f6cf68f5900d144efd08820',1,1,'authToken','[]',1,'2020-09-16 12:10:46','2020-09-16 12:10:46','2021-09-16 12:10:46'),('28bcea3f6c909d6f771053437bb7515587c43ba273cb3ba859b8554d8f8dfa16a0b2973f73f1a764',1,1,'authToken','[]',1,'2020-09-17 05:26:31','2020-09-17 05:26:31','2021-09-17 05:26:31'),('29ed35ba935a45eb0f8b6e467921962486fa68fb3706a0e30e69633fd81c52b0a5f660f564a2a179',1,1,'authToken','[]',1,'2020-09-16 17:14:18','2020-09-16 17:14:18','2021-09-16 17:14:18'),('2c381eb1960404eadc9e8821dec8154e78c17890ac6b79a9179d30dc99af0fa4e608d4ffed4dde33',1,1,'authToken','[]',1,'2020-09-16 13:34:01','2020-09-16 13:34:01','2021-09-16 13:34:01'),('2df5a7f3dbc59285c663adf2088b47e4d66279cdbf6545ac5677c0723399aba4c0a5da6319dc2799',1,1,'authToken','[]',0,'2020-09-18 04:29:04','2020-09-18 04:29:04','2021-09-18 04:29:04'),('2eb08204822f38e1c5846dd780379ec584e17260fe7727425106129fd943ba72dd573c672c32df99',1,1,'authToken','[]',1,'2020-09-16 17:21:34','2020-09-16 17:21:34','2021-09-16 17:21:34'),('2eb0c9161774dbcf4ede367a2bd2840f0b6b9a88bd017c25a052164e3fa9e053e8c82c43b8fcdbf9',1,1,'authToken','[]',1,'2020-09-16 17:12:16','2020-09-16 17:12:16','2021-09-16 17:12:16'),('2ecadf750fcacba7b80d8e0b766c55e22183ef0c296999e0c3de6d419940e02a3713372edc2a1741',1,1,'authToken','[]',1,'2020-09-16 15:02:58','2020-09-16 15:02:58','2021-09-16 15:02:58'),('2ed892dbd7323e255d5a0975fada64b8810add66dd5839d0abc1c35fef06e6a69c30a99d6bebb099',1,1,'authToken','[]',1,'2020-09-17 11:18:18','2020-09-17 11:18:18','2021-09-17 11:18:18'),('2ed920e90f66b6a059ccbc206cf8c12108f1dc372710196019c91ca6125961f13bda3302a11c4138',1,1,'authToken','[]',1,'2020-09-16 14:47:04','2020-09-16 14:47:04','2021-09-16 14:47:04'),('2efa3e13b05ff9493e38b54c058cfc8dbb4ef8b584df71af406812a3ed084e392eb8cc25764b0576',1,1,'authToken','[]',0,'2020-09-18 12:25:21','2020-09-18 12:25:21','2021-09-18 12:25:21'),('2fbaa9adff403ff89534a9579016a0c2363fbe74672d797ad34339e5449cf9a969076c430d6c78c7',1,1,'authToken','[]',0,'2020-09-18 12:18:19','2020-09-18 12:18:19','2021-09-18 12:18:19'),('30046231b025fbc847a77054c5ef1433c97ba5e8abe45ee5a7aa7d3d922c627968d47466c54b682b',1,1,'authToken','[]',1,'2020-09-16 13:48:33','2020-09-16 13:48:33','2021-09-16 13:48:33'),('3081a6325ff54f5651952b9ebeadff9c2b708e45dad1fc5dcb50dab41ffca6f72e0a2c3cff21bdeb',1,1,'authToken','[]',1,'2020-09-16 17:23:47','2020-09-16 17:23:47','2021-09-16 17:23:47'),('30b5c837e72201af6c2bd8a3c695217b26d6c93afbb3d3e419a301f00d43da85445a62b09845bcee',1,1,'authToken','[]',0,'2020-09-18 12:46:58','2020-09-18 12:46:58','2021-09-18 12:46:58'),('30cd69277840a645a95a1f1f056ecdf6da3552f60fedabb84050e20caec02d81c81bb2ead08f39cc',1,1,'authToken','[]',1,'2020-09-16 17:23:48','2020-09-16 17:23:48','2021-09-16 17:23:48'),('314ce6ba7353fa1ee99e01b5a85921285985b74933fb81036f550c1f0d1582d79e776c919227a340',1,1,'authToken','[]',1,'2020-09-16 17:34:12','2020-09-16 17:34:12','2021-09-16 17:34:12'),('3155f81ba05d7641e0e5510f117578cf89f68b9cdd28850fec1baaa826a73f436add7f6fa346660e',1,1,'authToken','[]',1,'2020-09-16 17:26:14','2020-09-16 17:26:14','2021-09-16 17:26:14'),('32167cc9d40fb188b860b449beaa6af4f76d076272086a2eef7bfdfa91bd96e816ed4e4e53a6cae7',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('32335a3ef357d0af7d64c33dafad036088080dc2160e316d69985a35393eae7180400b302f9c4293',1,1,'authToken','[]',1,'2020-09-16 17:18:13','2020-09-16 17:18:13','2021-09-16 17:18:13'),('328f0328b61e3dd5a12b91da3b5ad92bf84327c52d19f65b36ac32f9dc5584e0d9860117dc3bd219',1,1,'authToken','[]',1,'2020-09-17 01:54:42','2020-09-17 01:54:42','2021-09-17 01:54:42'),('340b7cb0d6985a0678334fe3d3010c764d0a582e0db8224babb26e3519cc6e914f44e749a4312b03',1,1,'authToken','[]',0,'2020-09-18 12:07:25','2020-09-18 12:07:25','2021-09-18 12:07:25'),('35cac9f35c05f9f38197ba0b9645ac4622f8ffa0c1deec07f005383ba0a72c59e511ffed4fbd551e',1,1,'authToken','[]',0,'2020-09-18 13:25:51','2020-09-18 13:25:51','2021-09-18 13:25:51'),('35d074bcf5257247d4ce2dcd36d0bae50c56e0edac5f11c99c2d64c5130b461f93ba2d0c56a059ae',1,1,'authToken','[]',0,'2020-09-18 14:16:27','2020-09-18 14:16:27','2021-09-18 14:16:27'),('3658bf21ade537f2e8c3b8d443907918afcb211369ca7f2116c9f2011d893470749b104d410a10d6',1,1,'authToken','[]',0,'2020-09-18 12:25:36','2020-09-18 12:25:36','2021-09-18 12:25:36'),('36996a08f32fb1ac26bd3888063d91afa70062ecc7982671bea5474f256e2f76bd80df724648eab0',1,1,'authToken','[]',1,'2020-09-16 13:44:19','2020-09-16 13:44:19','2021-09-16 13:44:19'),('375935527aa5ad761f24e7e7c58cc8c4eb14da3c7029f812a15d68919071a04d7360ab5ac11fb4d0',1,1,'authToken','[]',1,'2020-09-16 17:28:26','2020-09-16 17:28:26','2021-09-16 17:28:26'),('37b8f4d41eaac4582040059ea6cb573cb08089584e96a72093e7285d810353d261495d766291189a',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('3a7d16a47d83add329347feb04a3f58ea755eb3e092101f6a8871b585445a8d1162622a4266279d9',1,1,'authToken','[]',0,'2020-09-18 12:26:32','2020-09-18 12:26:32','2021-09-18 12:26:32'),('3ab5bfdb954c9224dcde94f9b1c3b8679aa962e8d72a258407aadd3c8e0ccf2502c75ac90e0fa4b5',1,1,'authToken','[]',1,'2020-09-17 01:46:52','2020-09-17 01:46:52','2021-09-17 01:46:52'),('3acd7ef0eab8c903fa9f60b9f9a5d0d34f8a2f51ecfd29d7bf7a22288b3ffa87ac03a8658bf728f4',1,1,'authToken','[]',1,'2020-09-17 01:43:10','2020-09-17 01:43:10','2021-09-17 01:43:10'),('3bef71884f383a1c8caba0739a5178d370e3b178c067d0e3ab4c4e37a16b23890041b633d403b515',1,1,'authToken','[]',1,'2020-09-16 17:12:11','2020-09-16 17:12:11','2021-09-16 17:12:11'),('3bf15751f60a972a8b713a246dfb843786366799747a5b31d7932bddf4eb4ecba8dd01dad9487ba7',1,1,'authToken','[]',1,'2020-09-17 01:45:34','2020-09-17 01:45:34','2021-09-17 01:45:34'),('3c9e3baad99fae5e781954ae525713c0d5e4ca5ead64220318af220073a9d680a7e53f926a4cb5cf',1,1,'authToken','[]',1,'2020-09-16 17:24:06','2020-09-16 17:24:06','2021-09-16 17:24:06'),('3cbdbbe4cfd88367cf216444fc28100cc02834406a62c686bfefc2fb4222044b0da7aecefaf5e365',1,1,'authToken','[]',1,'2020-09-16 13:58:24','2020-09-16 13:58:24','2021-09-16 13:58:24'),('3d48ad45e74525402e5c39e6e316b67c77d3ba6c7cbe79fe413e373cdda04dfc2a18f48dde3a9ea5',1,1,'authToken','[]',1,'2020-09-16 12:11:29','2020-09-16 12:11:29','2021-09-16 12:11:29'),('3e232cfda1fe140967da5b5c5e48f2d713795ab9e12f9eab0d16ca3968731a0e650de883ea3fea9d',1,1,'authToken','[]',1,'2020-09-16 17:35:51','2020-09-16 17:35:51','2021-09-16 17:35:51'),('3edd6db13b61c04d0decbbc9d9d477a17441b323b9c693101b3ff0039105ce39a6e660a0957ff6b4',1,1,'authToken','[]',0,'2020-09-18 01:01:47','2020-09-18 01:01:47','2021-09-18 01:01:47'),('3f532b50ccebea30e4aad9c627a3e234da5ecfea297236834fbe7822fde42ce69f05cd689ad19612',1,1,'authToken','[]',1,'2020-09-16 17:13:04','2020-09-16 17:13:04','2021-09-16 17:13:04'),('4059c8f9a5e9254cd66a85bf979d411ac23a47f5e6a67b8aa21f0a148f88fb8d54936d40d0e0bb9a',1,1,'authToken','[]',1,'2020-09-16 17:12:19','2020-09-16 17:12:19','2021-09-16 17:12:19'),('41831295e34020fd25d234d116b93cd1fa310c844f64aadf357db5b6a892243220f8d96f535e2597',1,1,'authToken','[]',1,'2020-09-16 15:03:12','2020-09-16 15:03:12','2021-09-16 15:03:12'),('447df5ce6d087b946a8410f09df328c9efdee68bc1ef056c9d606a71e81398933ae9cc166819d877',1,1,'authToken','[]',0,'2020-09-18 12:47:17','2020-09-18 12:47:17','2021-09-18 12:47:17'),('44ecefbbbc5f54500a2f9f15ba25b53341256cd4790296038af4e837e0e152d7f47dd747112b06de',1,1,'authToken','[]',1,'2020-09-16 14:47:04','2020-09-16 14:47:04','2021-09-16 14:47:04'),('4505a8f19410503b33535e40955b1182c9290e343ee0d8809582be1904f2898d6e5e0f74dfc82776',1,1,'authToken','[]',1,'2020-09-16 17:31:00','2020-09-16 17:31:00','2021-09-16 17:31:00'),('464b64f164be4dae382f99b469d51af886c51d4429a58f8aebb9d1192626c23144c5fc9552a78173',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('48a4b12ca32d0f8ebe591d4501ff50e071af06642037d4e3a6503fdb27c1aee77ba3fe5bb4a54d03',1,1,'authToken','[]',1,'2020-09-16 13:39:04','2020-09-16 13:39:04','2021-09-16 13:39:04'),('48db1c7669502e329b3e2979b4f300bf862b952c835e1eee5d8ad1c4df82da9618117caeea8ca816',1,1,'authToken','[]',1,'2020-09-16 13:35:02','2020-09-16 13:35:02','2021-09-16 13:35:02'),('497c24c8f4e39866c2feddece60b7760a2feca57fefb2e9c2c9180013fba54463f66f631ee783199',1,1,'authToken','[]',1,'2020-09-16 17:14:11','2020-09-16 17:14:11','2021-09-16 17:14:11'),('4aa75ac147623ee5e6c26400cdc884fcd7972c0d1c3cf083fed92e395f0532ac67688c958c7f5ef4',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('4c9ec28afab7a5c660d28d667c02fc03ad03ea798d9ae1cc8cc889f60ffaef5745e9b82abc3bd1d9',1,1,'authToken','[]',0,'2020-09-18 13:27:44','2020-09-18 13:27:44','2021-09-18 13:27:44'),('4d36a52761bbd1b805cf76c5ba2fa66cb5bbb777efc44ad6bf94a7c345d3da247758fb48ad11f060',1,1,'authToken','[]',1,'2020-09-16 13:48:59','2020-09-16 13:48:59','2021-09-16 13:48:59'),('4eef167a2fb4f5464fe96cda6043aeb66f122419c819f0de32705bdb43c3e3dfe75a363943809e8c',1,1,'authToken','[]',1,'2020-09-16 16:43:53','2020-09-16 16:43:53','2021-09-16 16:43:53'),('4f48774b1812b9ff0e86aacbdb9ceb8af2799a125b2995a1a1ff198c3dfc5a24e60bde85332ef753',1,1,'authToken','[]',1,'2020-09-17 01:45:39','2020-09-17 01:45:39','2021-09-17 01:45:39'),('4f89637b53db4ccf08fb66b84eacf7d834f67e172b9f5ea8d21432acc1f5d5e93321767e6a47dc1a',1,1,'authToken','[]',0,'2020-09-18 01:01:53','2020-09-18 01:01:53','2021-09-18 01:01:53'),('5139dd129777fb935959dfcf21ab5aca29ef9faa3ae590b8101a03423aad376463e5bcfc77145499',1,1,'authToken','[]',1,'2020-09-16 17:28:41','2020-09-16 17:28:41','2021-09-16 17:28:41'),('5293bc6a20b14f0f2a2c2f33c45072ab9a9eb66f5a33fbbb7c8f1ec8db91c17c4a19a37d8719958d',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('53fa19f4613d1f83a6157c9df8d282040d252baccae04093ff910dc3f12dd9c442d60bf5620dbde1',1,1,'authToken','[]',1,'2020-09-16 14:47:03','2020-09-16 14:47:03','2021-09-16 14:47:03'),('53fc7de937fb10afeeedf9a6b168aae1b138d274b94ecd0a984a539614f0e77dec707aa404c46d88',1,1,'authToken','[]',1,'2020-09-16 17:36:32','2020-09-16 17:36:32','2021-09-16 17:36:32'),('543e9a6f9a39ad17328bcbbae4aa1b07fdbb4e2a4d8d38ef833cf26259e23b8cfb10143e5072ba0f',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('544ad382b274d15b97ffa879a35237d4e04df39e442be5fe95484ff152eba570e4ee099912959be8',1,1,'authToken','[]',0,'2020-09-18 04:29:06','2020-09-18 04:29:06','2021-09-18 04:29:06'),('5550139653d6b45977b8460bb754a91695c337f486698f46ba04ebd4cab643ad8ee7284d1d82d379',1,1,'authToken','[]',1,'2020-09-16 17:14:24','2020-09-16 17:14:24','2021-09-16 17:14:24'),('55eaf83c57eff4f1e0b9accfd16c7c1f8b5293c428b07d1a5a777ec530c5bead6e19ca60abd0e4d1',1,1,'authToken','[]',0,'2020-09-18 12:07:10','2020-09-18 12:07:10','2021-09-18 12:07:10'),('57c1b5c6c6a9ac9911f20f422107b4f6c865ea48ad02f682d7b4f54bfc7c58338849df3c36861141',1,1,'authToken','[]',1,'2020-09-16 15:58:08','2020-09-16 15:58:08','2021-09-16 15:58:08'),('57f3651190c08dcb570fe4dab2a16252279f22b53ea895cc45fe9aee9ce984a3d20aa3c0fd92c653',1,1,'authToken','[]',0,'2020-09-18 12:11:06','2020-09-18 12:11:06','2021-09-18 12:11:06'),('5b07d0782571523b3f71d0a2ab98cbd8fdf3911426f8fe85ba60889dc218ea935707c99f7aa55bb2',1,1,'authToken','[]',0,'2020-09-18 12:07:11','2020-09-18 12:07:11','2021-09-18 12:07:11'),('5ce660de769b88748355e1f3132c292164bcf940bce341ea0ed48fe4306d5dab602a934872f5a480',1,1,'authToken','[]',1,'2020-09-16 17:28:59','2020-09-16 17:28:59','2021-09-16 17:28:59'),('5f47d40c646376ae9711eca93fc341246355cfb47cb6ee028704a8859ecd35fc29811e2fe749b3dd',1,1,'authToken','[]',1,'2020-09-16 13:48:52','2020-09-16 13:48:52','2021-09-16 13:48:52'),('5f7cecd81ea596ca3803f8285cfd3b3a3a6974c523ffae2f9ac6f31f43317a2614def568f3840d1d',1,1,'authToken','[]',1,'2020-09-16 13:54:14','2020-09-16 13:54:14','2021-09-16 13:54:14'),('5ff465d5de0b09a7d6f81c2534dda83f5d28ef40d566eb78e2f4bafbfc841ce0cdaea221fd5c4f55',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('62117bab80ed3531661ee07ef2acd84519b02b66f97db1f02fb34201ee2a6652747c993e40919c61',1,1,'authToken','[]',0,'2020-09-17 13:26:09','2020-09-17 13:26:09','2021-09-17 13:26:09'),('64d412a736dc96eb15bd96018a0a08dc2f86299c4a5273839176afd1309b7557c82552b6a6d025ac',1,1,'authToken','[]',0,'2020-09-17 13:26:13','2020-09-17 13:26:13','2021-09-17 13:26:13'),('650a1c4e91dbee5ef06c06d69f73cf1d86f1925c77e712826f9d8b4644fd9b97efcdf0901f32c2d2',1,1,'authToken','[]',0,'2020-09-18 01:01:58','2020-09-18 01:01:58','2021-09-18 01:01:58'),('651e3b62b0f6fb829c4ae3f9c5db5686d223b1621f66954411ced06c8523ecee6aad7565abb89813',1,1,'authToken','[]',0,'2020-09-18 12:10:03','2020-09-18 12:10:03','2021-09-18 12:10:03'),('68acdd250f2dab87d36ad97a978d6ad9fc42efa83c5bd94c3434e9ddbcbfe7c9f702e34192088301',1,1,'authToken','[]',1,'2020-09-16 17:16:12','2020-09-16 17:16:12','2021-09-16 17:16:12'),('690b4929520023af8caf443c00ee0ce0367930d048642c49e049c74cc8751a7499d98f38910f19fb',1,1,'authToken','[]',0,'2020-09-18 13:30:15','2020-09-18 13:30:15','2021-09-18 13:30:15'),('6942597233095706dda0125831921a9cdb6db73d734ab01e65d590ad3eec94b58c6f259756c9c44b',1,1,'authToken','[]',1,'2020-09-16 17:31:49','2020-09-16 17:31:49','2021-09-16 17:31:49'),('695266316ce1e7219cca2e262877d27d07983d2f77b88049b739b45716355e95fa9a9b51430edcc7',1,1,'authToken','[]',1,'2020-09-16 17:12:06','2020-09-16 17:12:06','2021-09-16 17:12:06'),('69b8c42dd4dd148f4e85a7a9e95aba506b3d097c26d903546f941ee446368c1706495bbf32a8d54e',1,1,'authToken','[]',1,'2020-09-16 13:39:03','2020-09-16 13:39:03','2021-09-16 13:39:03'),('6a3e26630c983da5bb5caf80de8fc067ad35ed5d7140db76d17b2120521a9d5d7727d17412660f28',1,1,'authToken','[]',1,'2020-09-17 01:43:37','2020-09-17 01:43:37','2021-09-17 01:43:37'),('6a51abc1dc383579c7573cbb16543cf70eb71a82b12adcea4faf275578d9c86aeffad69a8e0655c0',1,1,'authToken','[]',0,'2020-09-18 04:29:05','2020-09-18 04:29:05','2021-09-18 04:29:05'),('6adebece822b798f4313eee3c75c324e04c518665456ef4cce5e7f896b531bccb455c971ef1b69c0',1,1,'authToken','[]',1,'2020-09-17 02:00:37','2020-09-17 02:00:37','2021-09-17 02:00:37'),('6b898f12538258242afabf1443381bd02b8b3b1341a1e787c9320567bf5995c451fb893cc5157acb',1,1,'authToken','[]',1,'2020-09-16 17:28:57','2020-09-16 17:28:57','2021-09-16 17:28:57'),('6b8b70d54cb449d80e6af0c2653debdf5cd78082bd5453d8c80e73b179500d7525663a0d8c745bda',1,1,'authToken','[]',0,'2020-09-17 11:34:38','2020-09-17 11:34:38','2021-09-17 11:34:38'),('6cb24705687250f879df76b26572a562da6e59a8d2a5289177a16080a6a7014db8303af020a504fc',1,1,'authToken','[]',1,'2020-09-16 17:17:33','2020-09-16 17:17:33','2021-09-16 17:17:33'),('6d98c5d4b26d5f1776bffc4e4c6dd1981e864fe63c39ac1e06bf4d2baef2aee93ded357ea914ac84',1,1,'authToken','[]',1,'2020-09-16 16:43:55','2020-09-16 16:43:55','2021-09-16 16:43:55'),('6e3550924435f7512b2d3a8667c27d40a81200c3c9f428d162c7024527cbd709e2a122e44e085c7e',1,1,'authToken','[]',1,'2020-09-16 17:12:07','2020-09-16 17:12:07','2021-09-16 17:12:07'),('6e7630efbbf099db3d80aafa297caac35144ad870b6293cda574eaf527206a55ed03c083ca653d4e',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('6eb2d9204ce6094dbb2381175acb7846f4d0cf799d8c3e4458ee341f4777ac2344391c9f26a2cff5',1,1,'authToken','[]',1,'2020-09-16 13:34:54','2020-09-16 13:34:54','2021-09-16 13:34:54'),('6eb77ab5db1a8cfabb0a4d36bfab58dd2f5dc9f8a88427e74a4abed44c09c0cfd232ebc9011488f4',1,1,'authToken','[]',1,'2020-09-16 13:39:05','2020-09-16 13:39:05','2021-09-16 13:39:05'),('6f0573455a19446cd3a48cfe3c6d39b5799b101f4ca5d8640cde09443ccfda6cc7b690e6955d4ca3',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14'),('70987a61301580a5867ddbb83a7e4b04b3418e4c0bcc0ecbddfc738b9067f81f7f63708ca98b2dbb',1,1,'authToken','[]',1,'2020-09-16 17:13:07','2020-09-16 17:13:07','2021-09-16 17:13:07'),('735d46005113e1c68d1f56d854236027422154753db35d7a341a0b0df7a9c306447480b1bae1c6f5',1,1,'authToken','[]',1,'2020-09-17 01:47:04','2020-09-17 01:47:04','2021-09-17 01:47:04'),('73d6a61310bddcf161b90c44c750ec059a3378054878fca1ef14b12250a8698690ecd21237aca41a',1,1,'authToken','[]',1,'2020-09-16 15:02:58','2020-09-16 15:02:58','2021-09-16 15:02:58'),('7400cd513dd191e951f2e69e8d528989b550d3d50169ea5f646d8d615e28c1ef22c06b850c2f247c',1,1,'authToken','[]',0,'2020-09-18 12:21:04','2020-09-18 12:21:04','2021-09-18 12:21:04'),('76e404c216e79e96b4d1355c1a87c37aa1abb53b5594f0c9a687bbbbd06e0e69d482d6cb37a87eed',1,1,'authToken','[]',1,'2020-09-16 17:23:46','2020-09-16 17:23:46','2021-09-16 17:23:46'),('7982764a735d3c40d0fc08122ffa71df25593781ad670e0216ae72bcd903f1b2b56e900daca25873',1,1,'authToken','[]',1,'2020-09-16 13:46:06','2020-09-16 13:46:06','2021-09-16 13:46:06'),('7a0033fd16c58516f8dcb1ff6e0d4cdb123e0d20d124fb402322ebb308e2e80aa3c8cb0718de1b06',1,1,'authToken','[]',0,'2020-09-18 13:34:48','2020-09-18 13:34:48','2021-09-18 13:34:48'),('7a640a4bbf79101c1645dfcba14dc9ce5ed520495882110ef743129699c1ef4b5767497816dce9f2',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('7d051771d37745066e4f09073a9948560b1595e9e34d05d89d4172498171f98f4ade3bf16f2f9524',1,1,'authToken','[]',1,'2020-09-16 13:35:29','2020-09-16 13:35:29','2021-09-16 13:35:29'),('7da82871233f35e198248bef222c2428676d1798985ae99c4f8ec23a9e3ec7c03d61772a0005dcad',1,1,'authToken','[]',1,'2020-09-16 17:12:18','2020-09-16 17:12:18','2021-09-16 17:12:18'),('7ddf5e171074090fe97548a12261f5ebad9bf7a64bd4974d7ad4dbf7fe59c966eaff8d025f739caa',1,1,'authToken','[]',0,'2020-09-17 11:19:31','2020-09-17 11:19:31','2021-09-17 11:19:31'),('7f6b0b7d264916ff371a1042d8a6d96a6d3c8e8b40b4e6f7af1244235045669f8ef447313c622395',1,1,'authToken','[]',1,'2020-09-16 13:50:34','2020-09-16 13:50:34','2021-09-16 13:50:34'),('7fd6ca521c628206c5be8e23bc477c9e88ef3ead9514f790920e3faa5c48d24323868bfaf235af82',1,1,'authToken','[]',1,'2020-09-16 15:58:02','2020-09-16 15:58:02','2021-09-16 15:58:02'),('7ff1a0abfe1df2631c28325a6e3258d5797f3f3999777de6c954383e0f328683d1b7ec394647176c',1,1,'authToken','[]',0,'2020-09-18 01:01:51','2020-09-18 01:01:51','2021-09-18 01:01:51'),('80097cb28b97ee3be72e35f5173fcc5b46127eec3487f6f627aae4ed3b6580dc637ebaeb715dd903',1,1,'authToken','[]',1,'2020-09-16 17:17:04','2020-09-16 17:17:04','2021-09-16 17:17:04'),('80e4b17dc9d1fd7dc483a7b9a119abc4468ae4587942b8d68dc8f9c012e9798e76c4ef975e5dc848',1,1,'authToken','[]',1,'2020-09-17 01:43:20','2020-09-17 01:43:20','2021-09-17 01:43:20'),('810418e301c4425b868b0c6bbbb450bd70b3e8d335896a2e5019505f59db1a034214736362a064ea',1,1,'authToken','[]',0,'2020-09-18 13:35:27','2020-09-18 13:35:27','2021-09-18 13:35:27'),('81c82e1ecec7f50f7f3064ad6b71bbf6fc5f22995de2ad39298508eda9ab29269de6532a0dca8f96',1,1,'authToken','[]',1,'2020-09-16 17:23:50','2020-09-16 17:23:50','2021-09-16 17:23:50'),('8291cc5276f02f2507fa465b2aff171c0a830550a0f70660ccb05f76c55ae43c89367bf90b32ceeb',1,1,'authToken','[]',1,'2020-09-16 17:23:10','2020-09-16 17:23:10','2021-09-16 17:23:10'),('829bf67e2851106990601242ec776d2347d3580318ce4e1d055344ffc57d333c30369c8f217000c2',1,1,'authToken','[]',0,'2020-09-17 11:37:31','2020-09-17 11:37:31','2021-09-17 11:37:31'),('8346e537117272671fe396ff4542160d050b0006cf573d6ddf1e636f1adf58c4f044506b8a0ce772',1,1,'authToken','[]',1,'2020-09-16 13:48:59','2020-09-16 13:48:59','2021-09-16 13:48:59'),('8710318d3fc409876980623ed6101cbc237b67440cffe9594e467d1097c1caf3ac4f40a5e131ae52',1,1,'authToken','[]',1,'2020-09-16 13:57:05','2020-09-16 13:57:05','2021-09-16 13:57:05'),('8735f1ffc099054fed8f9a92e2c138d1c09426a28f19296d9d6e51d9d3670b5acc0b289ffbdbba1d',1,1,'authToken','[]',1,'2020-09-16 17:12:04','2020-09-16 17:12:04','2021-09-16 17:12:04'),('874c8324feac493077b5fbebdc7b257ed84c73f5e299e7e467bc8b26df71d79a84f9628295e00a8c',1,1,'authToken','[]',1,'2020-09-16 16:15:51','2020-09-16 16:15:51','2021-09-16 16:15:51'),('876639375984876ec6c08eac382ad2161d1c3b362ab4b8900ae50a765c3a5059a63f784537059228',1,1,'authToken','[]',1,'2020-09-16 17:12:01','2020-09-16 17:12:01','2021-09-16 17:12:01'),('88a55af10e93b6bfbb76bccf0384f457791e3698a2069ee0771586c69f618604c9f9849f000d3e84',1,1,'authToken','[]',1,'2020-09-16 13:48:52','2020-09-16 13:48:52','2021-09-16 13:48:52'),('88feb7a0d6729704c6daf82bc778891c35e8d8172b9b976eb69a7776bf839873e2b244187bd9a4e7',1,1,'authToken','[]',1,'2020-09-16 12:10:39','2020-09-16 12:10:39','2021-09-16 12:10:39'),('890862cc1f66473045ceaf0cd5c88dd9f0f39f43e35576a54802bdd60d69f65418b5608672969420',1,1,'authToken','[]',0,'2020-09-18 12:10:33','2020-09-18 12:10:33','2021-09-18 12:10:33'),('8a81be0f009d981caff8c495987d0de6d51a791e02dad8a4d48e877f726fa4a1507814f5365f2078',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('8b3aaa169b8602c23055ec79f2d2e06aaea5d3bb7e88c0cfc28db2a14666e7a6e1fac8c74d00732b',1,1,'authToken','[]',1,'2020-09-17 01:43:11','2020-09-17 01:43:11','2021-09-17 01:43:11'),('8c5961b055665e24a683c4cfa92983c61a2d3f8f812e056afab5ad6dd576888541caa4eee99d4f1b',1,1,'authToken','[]',1,'2020-09-16 13:46:06','2020-09-16 13:46:06','2021-09-16 13:46:06'),('8d43e4ce943b08683f4eed66de74f4c5977e50fcd50b65850b63edbdd5cb13de45f7a9cc2d8b3e8d',1,1,'authToken','[]',0,'2020-09-18 13:26:42','2020-09-18 13:26:42','2021-09-18 13:26:42'),('8e4ec67ed23cb0fa8a65a7b7fe207892ed804d74eed182d28e80c8fcca950046b8927ecc490fc2e1',1,1,'authToken','[]',0,'2020-09-18 12:24:00','2020-09-18 12:24:00','2021-09-18 12:24:00'),('90e03866c3b10351163a47d028b3bdde504d7ebd608506123ed293a3234a1051ae31dade8b68564c',1,1,'authToken','[]',0,'2020-09-18 12:07:13','2020-09-18 12:07:13','2021-09-18 12:07:13'),('9234d06978842dca1feececfb8b8afa3781cf35178edfdb26c42c1429a79148627784a0fb8e1b641',1,1,'authToken','[]',1,'2020-09-16 17:19:13','2020-09-16 17:19:13','2021-09-16 17:19:13'),('928cf065a99092b8b8260e132b1e0fa3611792fdd7b673bdbc7c4019d9083ddb0091afd3ab69e9c2',1,1,'authToken','[]',1,'2020-09-16 15:58:04','2020-09-16 15:58:04','2021-09-16 15:58:04'),('938cb0ec5b76c7d0e285b96c363cb025a8ebba4f6db2f532c1ffca26f7e4961b6a18384646470751',1,1,'authToken','[]',0,'2020-09-18 01:01:49','2020-09-18 01:01:49','2021-09-18 01:01:49'),('93da7718ffcd0fe7f946fd96d3aab5cf6e0ddc313e1ed54f8f2fe85808492580d85eeeb54a6d0704',1,1,'authToken','[]',1,'2020-09-17 01:59:31','2020-09-17 01:59:31','2021-09-17 01:59:31'),('940272e6b2414a4cc18aa1567873641071d1a653d6e071d49202c383a4d7e96c35273a572172699f',1,1,'authToken','[]',0,'2020-09-17 12:36:21','2020-09-17 12:36:21','2021-09-17 12:36:21'),('94669dfa60cbb5c0d8fe4916378f76dfc86f8d025e558ec68e7b24b34edecddf71054e04c71f0ba5',1,1,'authToken','[]',1,'2020-09-16 14:47:03','2020-09-16 14:47:03','2021-09-16 14:47:03'),('9519b8cf5de1eef1d38f200aba6fd46ac18ccccf1a37d4e7a68af5205338973bd17d3b15f232f692',1,1,'authToken','[]',1,'2020-09-16 17:24:02','2020-09-16 17:24:02','2021-09-16 17:24:02'),('960a4d1d56a616b36856e147db652faca3bc7fd4d7e2f1063a5927108c7a9952c908163bea7262b8',1,1,'authToken','[]',0,'2020-09-18 13:42:59','2020-09-18 13:42:59','2021-09-18 13:42:59'),('9631ba69647a7970b5aa5aa846753a26f087a906d05e46b7ec47f3e94c781b19543693e460e30479',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('967287983166b4d4bb81ec7d0610ed9c4b0b75c012777d5025f9949452af0c027e92eb8dc01f593d',1,1,'authToken','[]',1,'2020-09-16 17:28:36','2020-09-16 17:28:36','2021-09-16 17:28:36'),('967ceb7058029270151a3d94f92bec6d41cd2abfecc1151fb8ea0dd54b89cf5354593eaf7f47427c',1,1,'authToken','[]',1,'2020-09-16 17:12:08','2020-09-16 17:12:08','2021-09-16 17:12:08'),('96c8064b54edfb27d0a2a2e817ef0b166f9f44afd6205f6f5e84596d9f94901c4ae4dcb97db055a7',1,1,'authToken','[]',1,'2020-09-17 09:47:55','2020-09-17 09:47:55','2021-09-17 09:47:55'),('982d21eb10a44f459d88d30fc28595f6019a83e467229e941de1fd50cb8aef0d802c707b42c108e2',1,1,'authToken','[]',0,'2020-09-18 12:18:24','2020-09-18 12:18:24','2021-09-18 12:18:24'),('9867ed3b6e09299ae82f8c1d4453761184a551a1ddb6d9aa067d8fd05bedf806242751a31e95a441',1,1,'authToken','[]',1,'2020-09-16 17:13:27','2020-09-16 17:13:27','2021-09-16 17:13:27'),('9933b0a0a696043c97a53a351821a0fe38406559b610bbad4ab83ad8ce160506c62fb201617277b6',1,1,'authToken','[]',1,'2020-09-16 17:16:32','2020-09-16 17:16:32','2021-09-16 17:16:32'),('99c9404defe854bf5a8ecef65eec438803e65ef8e50b7999f514c6a36a9b2135c159950c4658e307',1,1,'authToken','[]',1,'2020-09-16 13:54:14','2020-09-16 13:54:14','2021-09-16 13:54:14'),('9abf490800aeab208f2b4f64028aa468effa63f8a51841d47e91b792d8fc58eeb7e487ce31ba454f',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('9be51892be025ec5f4c79a679fe975048ee3dcbda254da2c583cb6306d768476c4880996a2976d76',1,1,'authToken','[]',1,'2020-09-16 17:27:12','2020-09-16 17:27:12','2021-09-16 17:27:12'),('9c0200ce7db698d54cde356b0a6c328e3c7a82acceaed5597c2feface5efddab9cd32598bf973a1e',1,1,'authToken','[]',1,'2020-09-17 01:51:12','2020-09-17 01:51:12','2021-09-17 01:51:12'),('9c09b88a20a74ed045385c2998051bcea08a70297b4219eb6549d3160720c2dd0254b8783d4e30f4',1,1,'authToken','[]',1,'2020-09-16 13:49:13','2020-09-16 13:49:13','2021-09-16 13:49:13'),('9c3ae3d42197d316ed2085d00b9448be4d47a0b40ae94e95ac541578173bf0484a16dd50c494a597',1,1,'authToken','[]',1,'2020-09-17 11:05:13','2020-09-17 11:05:13','2021-09-17 11:05:13'),('9ca786966ca662178e3aef32725e01f2dbc6b698240bb1bbb44a6fac9409ee09e403d6069a635972',1,1,'authToken','[]',1,'2020-09-16 16:49:05','2020-09-16 16:49:05','2021-09-16 16:49:05'),('9d2baf8d12d32623ebb199e61ffee7f452dd3b4a4353643ea367f9776169fe4f78a835627e03c811',1,1,'authToken','[]',1,'2020-09-16 17:32:19','2020-09-16 17:32:19','2021-09-16 17:32:19'),('9de613717d92c03a646ec4f6ad3b8c0a78918d4f8bc508287912a5ca9e33ce55585543e48cc63d3c',1,1,'authToken','[]',0,'2020-09-18 12:27:55','2020-09-18 12:27:55','2021-09-18 12:27:55'),('9f01c54417e265bda396fcce5d1f6276ee192a30ab6adbb782e929b17aca3d298b2cfb9cfc37c3a3',1,1,'authToken','[]',1,'2020-09-16 13:54:29','2020-09-16 13:54:29','2021-09-16 13:54:29'),('9fbbede8d59aaa92e1ef6eebec2ae89724216edf99bce80e655dec9f35a1b0dd467ac9533e3cf62c',1,1,'authToken','[]',0,'2020-09-18 13:24:49','2020-09-18 13:24:49','2021-09-18 13:24:49'),('a081c2affb230b66860c6a5fa123caaf3d84d06446b184938bac13734346d4c73580d02bb48da7e9',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('a147e9c9ab8f0153b79a4d14e74f95aed45e0af46fe8a0139c9d4d8ef357f31854406d630c691baf',1,1,'authToken','[]',0,'2020-09-18 12:19:03','2020-09-18 12:19:03','2021-09-18 12:19:03'),('a19a567536714cb024709bf50b80b959f040da56d8c8d83ccbca8eec687ecd12051b4d3bfae3d797',1,1,'authToken','[]',0,'2020-09-18 01:01:53','2020-09-18 01:01:53','2021-09-18 01:01:53'),('a1aba7366fa8b32c287a71d7706f9dfc5217711fca3f1b16ffee5b86131fb6d2ce104eea0af004a6',1,1,'authToken','[]',0,'2020-09-18 13:43:31','2020-09-18 13:43:31','2021-09-18 13:43:31'),('a2911f8028ee670ff7874361e814948d4ad46b79f52e41244847020d24e4d4936ea5fe0e288493ab',1,1,'authToken','[]',1,'2020-09-16 13:56:03','2020-09-16 13:56:03','2021-09-16 13:56:03'),('a316c8e2d8ba194f2c979640d9ce06b806219520adc5c246b16ef713fdf49aec8ccae6d6146ced97',1,1,'authToken','[]',1,'2020-09-16 17:28:58','2020-09-16 17:28:58','2021-09-16 17:28:58'),('a3695a32a79e6569271c478618f374a38c5abe77cee8a50170f1c4a8e3d1dfc4dead7fc39dcf3839',1,1,'authToken','[]',1,'2020-09-16 13:40:28','2020-09-16 13:40:28','2021-09-16 13:40:28'),('a4f9a0f8796f327722cb75fd1009c20c9eeea072945d35f62fc6fed88729f76d9298f82c7c583c25',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('a53735b006cd8c3c9e8bad3ea7fc488ccbf75c81d276d475e8dec6bb93e16742273f60a34d186757',1,1,'authToken','[]',1,'2020-09-16 17:12:03','2020-09-16 17:12:03','2021-09-16 17:12:03'),('a5550e388eca6edc8178eed98ee943cf9c3cb733c5f812e5d843c966468c19f2a1acf6ca023ac6e2',1,1,'authToken','[]',1,'2020-09-17 01:59:19','2020-09-17 01:59:19','2021-09-17 01:59:19'),('a5fe6906a8fadd116400292bf483d744083c05a0fd8f39a86d4b21f057140919c0a9d93787820bf3',1,1,'authToken','[]',1,'2020-09-16 13:44:21','2020-09-16 13:44:21','2021-09-16 13:44:21'),('a60e551f3831b7195e2a5cf00c6f330f43ce7f57ab7ffd5f4ab626512049cbab4ddfae4dee27fa76',1,1,'authToken','[]',1,'2020-09-16 13:39:05','2020-09-16 13:39:05','2021-09-16 13:39:05'),('a61032c05e5fae0dced03d895482ef75d9b66b300b194d2abd44c6f4000d628d158155a2aafa7771',1,1,'authToken','[]',1,'2020-09-17 01:45:24','2020-09-17 01:45:24','2021-09-17 01:45:24'),('a746e5ca3b2083c5fec46eac4817700e0a40dca5ec2e08649acb6d18c6ea7b658e1da0147f5b9738',1,1,'authToken','[]',0,'2020-09-18 04:29:00','2020-09-18 04:29:00','2021-09-18 04:29:00'),('a76bf6a144cacd9d51ad11f64ee9c19f5fe14ab8e0ab7aff76a70ac5abd7195e54115a9905175c27',1,1,'authToken','[]',0,'2020-09-18 12:07:13','2020-09-18 12:07:13','2021-09-18 12:07:13'),('ab1d786bd62d6c312e546cc1820e7315d63692ef379c0f0a343951e3d407238ec1cc5da6413972a4',1,1,'authToken','[]',0,'2020-09-18 12:16:30','2020-09-18 12:16:30','2021-09-18 12:16:30'),('ac7b1e7c2dcdb99ef187a8327442b394911b4a21a2d792a5daff83add10c7fcaab212c300b0c7bb2',1,1,'authToken','[]',1,'2020-09-16 13:37:59','2020-09-16 13:37:59','2021-09-16 13:37:59'),('adb153da740bd123866bbf22a588abe66e87e715344953cefbf7fcd029eedb8433a4f58d4f42471e',1,1,'authToken','[]',1,'2020-09-16 15:58:06','2020-09-16 15:58:06','2021-09-16 15:58:06'),('ae072a3b8f3a9ffd59abc2b887353e9da0b82526808bee525a522f10f3a48b1bf2f8ff55b667141d',1,1,'authToken','[]',1,'2020-09-17 01:59:47','2020-09-17 01:59:47','2021-09-17 01:59:47'),('b06b802e7c34c51e969de772ace736dfad3acc433db1c074b7fd1ae91c782931fec1f1beadddb2dd',1,1,'authToken','[]',0,'2020-09-18 13:25:14','2020-09-18 13:25:14','2021-09-18 13:25:14'),('b1a9c68a97e8f15148f1582136450d5fa3b10808bd268623dafee0a75d43d67bc41bb6b6f1b05ad0',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('b29c2b4a77edde12efe6950e88dc1e054be4739d8b54d629c5482558273878348a661dea07a878b3',1,1,'authToken','[]',0,'2020-09-18 12:25:46','2020-09-18 12:25:46','2021-09-18 12:25:46'),('b2cf919fc1eb02ac2860c5c334a7ae77a526e9ab0c4ce965f84bce5d7b687eeab0ab41f4df0ac84f',1,1,'authToken','[]',1,'2020-09-16 17:13:03','2020-09-16 17:13:03','2021-09-16 17:13:03'),('b315abfe077b45ca7d2114eace8f89d9ad23e20db4990a673086af3e3d7b0e8aa4a12fde8b16dff1',1,1,'authToken','[]',1,'2020-09-16 13:50:34','2020-09-16 13:50:34','2021-09-16 13:50:34'),('b3190ccc424a475b70ad6671c62a752de6d96e0e0a0476479d91547c8a1517197bd87e26e52448f7',1,1,'authToken','[]',1,'2020-09-16 17:32:37','2020-09-16 17:32:37','2021-09-16 17:32:37'),('b3d4c2672558ad65f26f9a9ffae1c79ec4298b90c4277b2ef75f8d2722a925cef9fbedd1aa523af7',1,1,'authToken','[]',1,'2020-09-16 12:12:06','2020-09-16 12:12:06','2021-09-16 12:12:06'),('b4f4284d06b02ae2737c16a9221a9ee3ba52039dc07b4959ed1b21c272ba571fbbc14f692a1e0233',1,1,'authToken','[]',1,'2020-09-17 02:00:23','2020-09-17 02:00:23','2021-09-17 02:00:23'),('b6443f153e563782037d1b5e9e3f0d83a5f70259e6ca41aa17f4b3754ea8739c9459869aeefb45f1',1,1,'authToken','[]',1,'2020-09-16 17:12:15','2020-09-16 17:12:15','2021-09-16 17:12:15'),('b6503dd927ea26c4d9cca3cebba33e04265e47d1bea4a4c9375c8ee7016d5a589c3262cda74fff68',1,1,'authToken','[]',0,'2020-09-18 12:10:37','2020-09-18 12:10:37','2021-09-18 12:10:37'),('b7821e18abf0bc621c6e93fcdda3f0019eb3ad33a9b1c665e8f8c632b9886ad3e27d5cd419bd7f64',1,1,'authToken','[]',0,'2020-09-18 13:30:59','2020-09-18 13:30:59','2021-09-18 13:30:59'),('b812d6b0a83b4dd3d221f5def614a2328002fcb30e04ebf54a0e5fabf85f53f805763a4eb4994420',1,1,'authToken','[]',1,'2020-09-16 17:16:46','2020-09-16 17:16:46','2021-09-16 17:16:46'),('b85843db7ad7795e0f8074a00a95f00da22ee4fb8d6e5f36806897f6a0787d252ae698d7caeb0d75',1,1,'authToken','[]',1,'2020-09-16 13:47:19','2020-09-16 13:47:19','2021-09-16 13:47:19'),('b86b9c5c572a9bd10d143fe708b0d9005147ec31276cc239535354ffe5da845409e6eee45e31b057',1,1,'authToken','[]',1,'2020-09-17 01:55:02','2020-09-17 01:55:02','2021-09-17 01:55:02'),('b957f2beb5bdf2f7ba2bdc4ee731570f021d1c13413070f1eeecd86367346b8fb30dfc50b5339b35',1,1,'authToken','[]',1,'2020-09-16 13:49:13','2020-09-16 13:49:13','2021-09-16 13:49:13'),('bacd386d5e2c09429d0ae65cfa7202a49be244ed42e5d232d158cd0c1381cc7d617c4b2a7ab5e292',1,1,'authToken','[]',1,'2020-09-16 13:35:29','2020-09-16 13:35:29','2021-09-16 13:35:29'),('baf78be83e6ab8e13dc698f65dd261940ec0f14a84196768eddf4bd9d65682c0138e98acbacb9858',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('bb22a75458d5400e1b7fc48fb2ebdfbafd1ec119b891c3e276833945e3e7b0a8b2ad35cf5dd5101a',1,1,'authToken','[]',0,'2020-09-18 12:19:21','2020-09-18 12:19:21','2021-09-18 12:19:21'),('bb59b622b035289f95a2912cf16748eb3ec5906e834884b07141ef1d981d2df2bb4fccaffb577ba1',1,1,'authToken','[]',1,'2020-09-16 17:20:07','2020-09-16 17:20:07','2021-09-16 17:20:07'),('bb59c56b08812f538237170c6551d517632039b2b63c2f7ac6217fc10494cf191f9b9465c7563195',1,1,'authToken','[]',1,'2020-09-16 17:12:09','2020-09-16 17:12:09','2021-09-16 17:12:09'),('bc21381a9228ad6cb5cc1bc0d9d4f3d88341210c01618703739df03c93aa7e298f612f4d711c208a',1,1,'authToken','[]',1,'2020-09-16 17:32:59','2020-09-16 17:32:59','2021-09-16 17:32:59'),('bdbf54c7aded9485d3f2d87e8912b60d3e9cf5f93b76ad3b0b24a182a61afb34011bab2a38b52b37',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14'),('bfb4cc888becfa5bf11388bd9d427eb9d390d5076c196a0eaeab80316bccc7623d54e9193a5a9058',1,1,'authToken','[]',1,'2020-09-16 13:48:34','2020-09-16 13:48:34','2021-09-16 13:48:34'),('c213e69dfb8cd607dcbd99c7624dee1c87f6305868ccc377f21342ea9f2568410c43f7330c502b2b',1,1,'authToken','[]',1,'2020-09-16 17:24:05','2020-09-16 17:24:05','2021-09-16 17:24:05'),('c4751406f180335040bb48f728dccbe12a65b192c2b3000f9851669069bbe4898b099725cff58ad4',1,1,'authToken','[]',1,'2020-09-16 17:13:09','2020-09-16 17:13:09','2021-09-16 17:13:09'),('c493474bfd0e70468683df59acf6213c6fa1fb009799fab839c2d97503a6bc1adac43080bec10268',1,1,'authToken','[]',1,'2020-09-16 17:36:21','2020-09-16 17:36:21','2021-09-16 17:36:21'),('c4b332825b1298075308d02429317e14e8ac4e186061076582de8e1673e6ceafc662fc6d5b869186',1,1,'authToken','[]',1,'2020-09-16 17:22:05','2020-09-16 17:22:05','2021-09-16 17:22:05'),('c543f95ed2027dd8eed0f60dfbf4cb91a9522938221173c9b2fa1e51c45b405a9c4061307d027479',1,1,'authToken','[]',1,'2020-09-16 17:36:50','2020-09-16 17:36:50','2021-09-16 17:36:50'),('c5a55efbde195c47037f966cfdc09ffcdf336f58d27aa9fa4a7310b2de0c11069a94fa56fa89aa2a',1,1,'authToken','[]',1,'2020-09-16 13:39:03','2020-09-16 13:39:03','2021-09-16 13:39:03'),('c64ad2a03599948957bcdb90652c7b753b2f3c56f42935b7392a32340946966b0c52b6423ee3885e',1,1,'authToken','[]',1,'2020-09-16 17:23:49','2020-09-16 17:23:49','2021-09-16 17:23:49'),('c6755356a535a95482e90a8434de5f6bf6a170c31c13efce73d6728ee724e48d8bdeae9c13bae7a7',1,1,'authToken','[]',1,'2020-09-16 17:28:55','2020-09-16 17:28:55','2021-09-16 17:28:55'),('c701f36e9a2d2c72ce4ce5be65442b57248fa11b293b93a067c500e3250629c9a16c50b73b7f3ea1',1,1,'authToken','[]',1,'2020-09-16 15:03:12','2020-09-16 15:03:12','2021-09-16 15:03:12'),('c74cb5e3142f0ec6d759a403898248c01df2ceaa6ce7cbfaed2613819023986d55b643e820a98b59',1,1,'authToken','[]',1,'2020-09-16 17:26:53','2020-09-16 17:26:53','2021-09-16 17:26:53'),('c7a7d42d935dfbe72f1d2ae8ba26b313ded3ec3a2c1c7121b77d089495dd2246dca49b8edd4e1fa1',1,1,'authToken','[]',1,'2020-09-17 01:59:10','2020-09-17 01:59:10','2021-09-17 01:59:10'),('c7cfbaebe7242c96390b2cc9c3431e89d69be48f27b12590df7278de1574da78554b306ce392703c',1,1,'authToken','[]',1,'2020-09-16 17:12:12','2020-09-16 17:12:12','2021-09-16 17:12:12'),('ca5e7889ee11779737e0c081ccf26c18231dec7256c67cbf42676264d80cb1f5e97227ab3845766a',1,1,'authToken','[]',1,'2020-09-17 01:43:12','2020-09-17 01:43:12','2021-09-17 01:43:12'),('ca7c4cbd3603562fd745d2f42d9bd960a8bc588ec645fb0cf15392c78589c5cff863c25cb19c38db',1,1,'authToken','[]',0,'2020-09-17 11:18:25','2020-09-17 11:18:25','2021-09-17 11:18:25'),('cb506ef003c2691474fb55124cbe801bed10548a9c6fc944a9b648822f3179ee49412f503a720cd5',1,1,'authToken','[]',1,'2020-09-17 01:55:18','2020-09-17 01:55:18','2021-09-17 01:55:18'),('ccc35074c6aed80fab9eaf9715881f2c65f77045aa00c1eef726f98ac3675a2e289bdae4da21dcc2',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('cdc7b865b813cca33f58ebe799a51041ea9fad16846ade8af377b1498e36bc4e5c5269ecd32842a5',1,1,'authToken','[]',1,'2020-09-16 16:43:55','2020-09-16 16:43:55','2021-09-16 16:43:55'),('cfba06fd96d0f7cd8252b3aaba276fd02d494c3d563219a29d3eff74f5027ca8d76b58ccb0d3907c',1,1,'authToken','[]',1,'2020-09-16 16:37:13','2020-09-16 16:37:13','2021-09-16 16:37:13'),('cfea319a570dd023ea984082367c9d5fc975f4d379ee30bbe232f42c5d990aa4f74152392d939ac3',1,1,'authToken','[]',0,'2020-09-18 12:24:26','2020-09-18 12:24:26','2021-09-18 12:24:26'),('d137311bb6eff8700dee42762b75312db943f407745992ae0ba1652e8534e31174211a63605de189',1,1,'authToken','[]',1,'2020-09-16 13:37:59','2020-09-16 13:37:59','2021-09-16 13:37:59'),('d14e4d0bb6935916d4dc8e52e68605709cae87480bf074f18025f8716cc37a0ba341c87571b254ac',1,1,'authToken','[]',0,'2020-09-18 01:01:55','2020-09-18 01:01:55','2021-09-18 01:01:55'),('d1b0dc18b731c8e340e2bc7cac803d512641341c882bfd8c95e2b9a2504c223a7b61368171d8a7ed',1,1,'authToken','[]',1,'2020-09-16 17:24:02','2020-09-16 17:24:02','2021-09-16 17:24:02'),('d2a6f5e5c658082b5ea9b127929f8159b1ee93195eb55dcad2b9847e1454c148f22ee4ebb624f862',1,1,'authToken','[]',0,'2020-09-18 01:01:52','2020-09-18 01:01:52','2021-09-18 01:01:52'),('d412b03dca044eed0cb66278a08e37b733179a472720d9f48722237465086a7fe1fc62697136bf88',1,1,'authToken','[]',1,'2020-09-17 01:43:08','2020-09-17 01:43:08','2021-09-17 01:43:08'),('d75a486bdd7b2f741cc940fff0d4cecfaab31a4c9d172e77e774446955a3b4aa877287433a0765d8',1,1,'authToken','[]',1,'2020-09-16 13:58:25','2020-09-16 13:58:25','2021-09-16 13:58:25'),('d7dec7e1dca7634cda7db97e4b187fdcf7fc60e59cdf9dba2b9017cef02f3acf8f70ab8eaf483186',1,1,'authToken','[]',0,'2020-09-18 12:07:10','2020-09-18 12:07:10','2021-09-18 12:07:10'),('d9b091b946ff954da5e9567557d19383149e2667fb9424d1dcb215291e3f2aeaa5206185a5c6c967',1,1,'authToken','[]',1,'2020-09-16 14:41:30','2020-09-16 14:41:30','2021-09-16 14:41:30'),('da0281ca8d5d82378a5e24122a9029e5f06abc120ae53e064a3ccbe123c4ca99a93a3bdf2eade5c1',1,1,'authToken','[]',1,'2020-09-17 01:58:55','2020-09-17 01:58:55','2021-09-17 01:58:55'),('da1acd243ba9f453073b8ca5613e54561f562195503632177bb89980a4a7e00bad9977e8fc05b15c',1,1,'authToken','[]',1,'2020-09-16 17:27:34','2020-09-16 17:27:34','2021-09-16 17:27:34'),('dba7d6e7b0e525d6447519bc51ba796971a0685c14f07dbe010f9c51a357d935cb40b840b67f5e42',1,1,'authToken','[]',0,'2020-09-18 01:01:59','2020-09-18 01:01:59','2021-09-18 01:01:59'),('dc8c79f3f6c709dd8498d567bf295c32b75c44dd8f98c4f75e5b4b579c5133c11edcbab36181ad84',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('dd04cab3779e46eab0dcf9caae2d5ef936f8ff991c80af3f05e4dca7198805898aa5dd28a6ee94c3',1,1,'authToken','[]',0,'2020-09-18 01:01:54','2020-09-18 01:01:54','2021-09-18 01:01:54'),('de50ca894bcf9a96bde0a547521e086d9697615cedb1383cab3d194a2ed6851e1e04302c5ca7aee2',1,1,'authToken','[]',1,'2020-09-16 16:49:09','2020-09-16 16:49:09','2021-09-16 16:49:09'),('e10bf9f9e81544e2f09d8a4f5040eed91d274a2c4c7519dbd867ec3fd2c94b949f890fa5aeabdd01',1,1,'authToken','[]',0,'2020-09-17 11:37:30','2020-09-17 11:37:30','2021-09-17 11:37:30'),('e15d8717645bbec1690565547c21689c97d7c3b23b3790efffb24d2725387ab32cf106cbdc9ad2ac',1,1,'authToken','[]',1,'2020-09-16 17:28:16','2020-09-16 17:28:16','2021-09-16 17:28:16'),('e3eb63b6d5f7afed543805ff03a6e20f2274e748fac66c88891443409bf9b7e206297bb9fe61776a',1,1,'authToken','[]',1,'2020-09-16 16:35:02','2020-09-16 16:35:02','2021-09-16 16:35:02'),('e52246688f90e6ede73a5ebcc3e8e362a72d7fb6b9f4d9f3c723a3f40261b3d7cf133e430690a215',1,1,'authToken','[]',0,'2020-09-17 11:35:19','2020-09-17 11:35:19','2021-09-17 11:35:19'),('e76a2a2584ed2c710e676bc237acec74957e8349ac191d6a0464b4862219b649eef689166c67a7b2',1,1,'authToken','[]',1,'2020-09-16 13:34:00','2020-09-16 13:34:00','2021-09-16 13:34:00'),('e78fe3efe808402da8e9fc327899e748b030ae1f4d6c633564ebdc405d720e954cd2811c4a71fab0',1,1,'authToken','[]',0,'2020-09-18 12:24:44','2020-09-18 12:24:44','2021-09-18 12:24:44'),('e900053d8cd05cce64ee443bfe085beec19f40a65ae009ba89abc82d7531452643a78c7033bc4024',1,1,'authToken','[]',0,'2020-09-18 13:27:19','2020-09-18 13:27:19','2021-09-18 13:27:19'),('e93daa83b33cf54fa118aceb355675baabbdfad32f1ca75f45d5377bf983c5a7b198bb98c98ffa76',1,1,'authToken','[]',1,'2020-09-17 01:54:59','2020-09-17 01:54:59','2021-09-17 01:54:59'),('eafb7498805c2ccb4d2a6295c663ff1d2b5eae60d3c6a654280a06321ad10515a9ddf52446a741ba',1,1,'authToken','[]',0,'2020-09-18 12:20:27','2020-09-18 12:20:27','2021-09-18 12:20:27'),('ec0c8e946c73186d9b25a545d24f2a9f0c5e25ff4d9a56dc99afa93b010dac04b598852540bbea72',1,1,'authToken','[]',0,'2020-09-18 12:26:52','2020-09-18 12:26:52','2021-09-18 12:26:52'),('ed5aab726adc345c79533f6e5551d01542cd34397d886cd10428c7650e8e0ac238308b3ae406a2f9',1,1,'authToken','[]',1,'2020-09-16 13:48:33','2020-09-16 13:48:33','2021-09-16 13:48:33'),('ee73f06112e38053e6ff01fd9a9f692a4e6428dc3ce9b09cc243cdb2a718f16f9e984e34503e10c6',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('eed80891a1da0d5b8568e57d7837addfe5a7950496ff2d22490e429027c175e10f20b65affd668fc',1,1,'authToken','[]',0,'2020-09-18 12:25:14','2020-09-18 12:25:14','2021-09-18 12:25:14'),('eef58b660c082682927f4e7b34f2476ed37dbbe5600d78fafaa6d56a0b76ef82815eae8c1adfed41',1,1,'authToken','[]',0,'2020-09-18 13:25:33','2020-09-18 13:25:33','2021-09-18 13:25:33'),('ef72023e6d5f84f434db92b8005e65703acbbb1b7d1a846135a8aaba71da44fd7c1acb5501df8abf',1,1,'authToken','[]',1,'2020-09-16 17:13:00','2020-09-16 17:13:00','2021-09-16 17:13:00'),('efafaac61a5d322ebfaef62285d4693eaad61b0c48a1a377070806e8f5c95d9bdf978ce732913b51',1,1,'authToken','[]',0,'2020-09-18 12:28:24','2020-09-18 12:28:24','2021-09-18 12:28:24'),('efb42eab8433b404b642cc73c24f6fdb74448f8ccd0370d7895574f200d227a3db6b06cb40427676',1,1,'authToken','[]',0,'2020-09-18 01:01:58','2020-09-18 01:01:58','2021-09-18 01:01:58'),('effc3e046bbbf34e979e12eb1d17a3ee67783bafbd8f2800391787138b5d5edb9efbf3f32a45ab01',1,1,'authToken','[]',1,'2020-09-17 01:43:46','2020-09-17 01:43:46','2021-09-17 01:43:46'),('f1814a2b00dac039edcc1743c63300e49041d82cb18037392c8e8ac94dd84f9a8c9fcc0e3016e95b',1,1,'authToken','[]',1,'2020-09-16 12:10:38','2020-09-16 12:10:38','2021-09-16 12:10:38'),('f22fdf5a645882bd2bfec20ace15e5348984cd87b5285d070786de63521a90ab3e097a8c410d272f',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('f28c540d7f190bf3ba5539b87e98ed21cac681a2dd850fbb5e0b206a751e93ea81c2be3587a04c66',1,1,'authToken','[]',1,'2020-09-16 17:15:54','2020-09-16 17:15:54','2021-09-16 17:15:54'),('f2fa314fa964115708d93251fcc2e675398137412b52abf484b3a2899a648f3323db29fb12e7c8b0',1,1,'authToken','[]',0,'2020-09-18 12:10:16','2020-09-18 12:10:16','2021-09-18 12:10:16'),('f31b6f5cfb01923d3970141efbb8c47cdc37b3ae3869f6bb6ee2fa30818fdb54c858d99343464fb8',1,1,'authToken','[]',1,'2020-09-16 17:26:56','2020-09-16 17:26:56','2021-09-16 17:26:56'),('f520c971e50f8f17e12d3775c28188a4edfd3c920e711e03fb9f02cb19c6183f8ed6a864485793db',1,1,'authToken','[]',0,'2020-09-17 11:19:01','2020-09-17 11:19:01','2021-09-17 11:19:01'),('f55ed46580165f131714733115df3e4885195a742c0d1cd64008654f6dc25ad741cd09c4686618a8',1,1,'authToken','[]',1,'2020-09-16 14:41:30','2020-09-16 14:41:30','2021-09-16 14:41:30'),('f5eb225078034f6ed8276bdd0d1f68a73059a667bc3c8501c33bc13135b89da94050f0f5503779f3',1,1,'authToken','[]',0,'2020-09-18 13:26:11','2020-09-18 13:26:11','2021-09-18 13:26:11'),('f6f7aec1fd9f4f64f09171b6adb0f9a41e22f5d82041ecba8f4416d48ea40560261c1a9aae7ea2cc',1,1,'authToken','[]',1,'2020-09-17 09:39:15','2020-09-17 09:39:15','2021-09-17 09:39:15'),('f7879f65eb48174d1005be5ad249e0341ac7113f8a3740ebb3b76f5bc2735201c81ad79c50c3af68',1,1,'authToken','[]',1,'2020-09-16 13:48:34','2020-09-16 13:48:34','2021-09-16 13:48:34'),('f8339b4d578579111c5995c01c2640791f9c2faf6e5feec65a452591334e5108f87147bf181962f6',1,1,'authToken','[]',1,'2020-09-16 17:14:29','2020-09-16 17:14:29','2021-09-16 17:14:29'),('f8547be0abf8bf1a00becb9cf57fb70e0b30d66d4aa4f4f4f8d42c747d9abfd2ebd340ba9da1470b',1,1,'authToken','[]',1,'2020-09-16 17:22:25','2020-09-16 17:22:25','2021-09-16 17:22:25'),('f88327ee0a827a7a5e4d0236c895fd2001b8fd5f6c73ae4923c611bd3cd7c085023eafd2646ae2ac',1,1,'authToken','[]',0,'2020-09-18 01:00:29','2020-09-18 01:00:29','2021-09-18 01:00:29'),('f8fefb65e1698a24c4db68c90bfdfa55cd5b508814cba633ed33a1a76f5d2cb240ac21df8e789732',1,1,'authToken','[]',1,'2020-09-17 01:52:53','2020-09-17 01:52:53','2021-09-17 01:52:53'),('f90b734d5df15d5da24775862e7a728f66acace5bdae1eb612978783c7a0cdab5a7dae9d577225dd',1,1,'authToken','[]',0,'2020-09-18 12:20:37','2020-09-18 12:20:37','2021-09-18 12:20:37'),('f9779fcd3fe881fdec30f4579e9ae1f27e23ec68d0d03bcc37960c08911841fcff95b6b77cf9b0c7',1,1,'authToken','[]',1,'2020-09-16 13:47:18','2020-09-16 13:47:18','2021-09-16 13:47:18'),('f9985da87cccede5c06fc12432473e159de50f6c617c92ac56564c0b6515a6f217144a7c92f056db',1,1,'authToken','[]',1,'2020-09-16 17:23:48','2020-09-16 17:23:48','2021-09-16 17:23:48'),('fa2d79566ca93cf904d41ba85c0aa53f643673e7a09736c6fdedb426203a557c6396ab7935aa2bf2',1,1,'authToken','[]',1,'2020-09-17 01:49:10','2020-09-17 01:49:10','2021-09-17 01:49:10'),('fa6ff7f5282afe0ae4708c23f5e7c26794727e96c6e995d02d7a4830d2cab394b72dc481876b4a25',1,1,'authToken','[]',1,'2020-09-16 15:58:09','2020-09-16 15:58:09','2021-09-16 15:58:09'),('fa980e9e76e49971ffd3a12b65d15b0d8138b49b623764ed1eb08a0a599f46a5be95e0e79b947467',1,1,'authToken','[]',0,'2020-09-18 12:20:52','2020-09-18 12:20:52','2021-09-18 12:20:52'),('fba7bfa8b2d6b683260feca33656ea50de73383aaf94cd5e9c5388da1bca95da8498535ecec83df6',1,1,'authToken','[]',0,'2020-09-18 01:01:51','2020-09-18 01:01:51','2021-09-18 01:01:51'),('fbcbf29d6cb10cc2177c21b0de5edbdaa4107ff99f34cd881b94c25e7f628f5eb1ab6588e8217bdc',1,1,'authToken','[]',0,'2020-09-17 11:18:27','2020-09-17 11:18:27','2021-09-17 11:18:27'),('fd0cfe9278463496b37c40f49d196755091cecea5ab8f9bc25da120fd7ad818a92dbacd4d31d609b',1,1,'authToken','[]',0,'2020-09-18 14:36:21','2020-09-18 14:36:21','2021-09-18 14:36:21'),('ffd27e2de1b52ab48dd90c50a85288979975b5e55a20ff6b40f73cbff1fd181a9020e6aa476a0d2e',1,1,'authToken','[]',1,'2020-09-16 17:12:14','2020-09-16 17:12:14','2021-09-16 17:12:14');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `client_id` bigint(20) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Online Betting Personal Access Client','Kyjjhb3nGGBOUccysf1ogvBKJqTql5LKSpaWRuhc',NULL,'http://localhost',1,0,0,'2020-09-16 12:09:57','2020-09-16 12:09:57'),(2,NULL,'Online Betting Password Grant Client','IKrqI4r70BfbW9awamgtGDM39BJkYbWOMO0fCOHf','users','http://localhost',0,1,0,'2020-09-16 12:09:57','2020-09-16 12:09:57');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2020-09-16 12:09:57','2020-09-16 12:09:57');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status_types`
--

DROP TABLE IF EXISTS `status_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status_types`
--

LOCK TABLES `status_types` WRITE;
/*!40000 ALTER TABLE `status_types` DISABLE KEYS */;
INSERT INTO `status_types` VALUES (1,'On-going',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(2,'Ended',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10'),(3,'Upcoming',NULL,NULL,'2020-09-16 11:36:10','2020-09-16 11:36:10');
/*!40000 ALTER TABLE `status_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@admin','admin@admin',NULL,'$2y$10$EUZhrpM2Na4PJ/uAQ8/j0uY9hMLRkBo.kbrw34374Fu1fULfpmwB2','5f6200a043c65','t6ugTqNHG9UK7CF3fMlAOVkIGHTTUmk2MYlNaccNxVjZv55zrzv1BiMtN4tU','2020-09-16 12:10:08','2020-09-16 12:10:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `winners`
--

DROP TABLE IF EXISTS `winners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `winners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `game_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `bet_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `winners_game_id_foreign` (`game_id`),
  KEY `winners_user_id_foreign` (`user_id`),
  KEY `winners_bet_id_foreign` (`bet_id`),
  CONSTRAINT `winners_bet_id_foreign` FOREIGN KEY (`bet_id`) REFERENCES `bets` (`id`),
  CONSTRAINT `winners_game_id_foreign` FOREIGN KEY (`game_id`) REFERENCES `games` (`id`),
  CONSTRAINT `winners_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `winners`
--

LOCK TABLES `winners` WRITE;
/*!40000 ALTER TABLE `winners` DISABLE KEYS */;
/*!40000 ALTER TABLE `winners` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-20  0:50:45
