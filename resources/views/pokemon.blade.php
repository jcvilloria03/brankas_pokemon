@extends('layouts.app')

@section('title', 'Announcements')

@section('content')

@guest
@else
<div class="row">
</div>
<br>
<div class="row justify-content-center">
    <div class="col-md-12">
        <pokemon-index :auth_user="{{Auth::user()}}"></pokemon-index>
    </div>
</div>
@endguest

@endsection