<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PokemonsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function show()
    {
        return view('pokemon');
    }

    public function index(Request $request)
    {
        if($request->name != ""){
            if ($request->name == trim($request->name) && strpos($request->name, ' ') !== false) {
                $json = file_get_contents('https://api.pokemontcg.io/v2/cards?page='.$request->page.'&pageSize=20&q=name:"'.$request->name.'"');
            } else {
                $json = file_get_contents('https://api.pokemontcg.io/v2/cards?page='.$request->page.'&pageSize=20&q=name:'.$request->name);
            }
        } else if($request->type != "Select Type"){
            $json = file_get_contents("https://api.pokemontcg.io/v2/cards?page=".$request->page."&pageSize=20&q=types:".$request->type);
        } else if($request->rarity != "Select Rarity"){
            $json = file_get_contents("https://api.pokemontcg.io/v2/cards?page=".$request->page."&pageSize=20&q=rarity:".$request->rarity);
        } else if($request->set != "Select Set"){
            $json = file_get_contents("https://api.pokemontcg.io/v2/cards?page=".$request->page."&pageSize=20&q=set.name:".$request->set);
        } else {
            $json = file_get_contents("https://api.pokemontcg.io/v2/cards?page=".$request->page."&pageSize=20");
        }

        return $json;
    }

    public function getType(Request $request)
    {
        $item = [];
        $json = file_get_contents("https://api.pokemontcg.io/v2/types");
        $json_arr = json_decode($json);
        array_unshift($json_arr->data, "Select Type");
        foreach ($json_arr->data as $key => $value) {
            $item[$key]["item"] = $value;
        }
        $json = json_encode($item);
        return $json;
    }

    public function getRarity(Request $request)
    {
        $item = [];
        $json = file_get_contents("https://api.pokemontcg.io/v2/rarities");
        $json_arr = json_decode($json);
        array_unshift($json_arr->data, "Select Rarity");
        foreach ($json_arr->data as $key => $value) {
            $item[$key]["item"] = $value;
        }
        $json = json_encode($item);
        return $json;
    }

    public function getSet(Request $request)
    {
        $item = [];
        $json = file_get_contents("https://api.pokemontcg.io/v2/sets");
        $json_arr = json_decode($json);

        foreach ($json_arr->data as $key => $value) {
            $item[$key]["item"] = $value->name; 
        }
        array_unshift($item, array('item' => 'Select Set'));

        $json = json_encode($item);
        return $json;
    }

    //https://api.pokemontcg.io/v2/cards?q=set.name:generations subtypes:meg
    //https://api.pokemontcg.io/v2/types
    //https://api.pokemontcg.io/v2/rarities
    //https://api.pokemontcg.io/v2/sets?q=legalities.standard:legal
}
